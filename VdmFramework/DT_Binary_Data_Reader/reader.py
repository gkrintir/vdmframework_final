import csv
import pandas as pd


# read flash.dat to a list of lists
datContent = [i.strip().split() for i in open("./scout.dat").readlines()]

# write it as a new CSV file
with open("./output.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerows(datContent)
