#include "Creader.h"
#include "json.hpp"
#include <iostream>
#include <stdio.h>
#include <iomanip>
#include <stack>
#include <vector>
#include <fstream>

using json = nlohmann::json;

int main(void){

FILE *f=fopen("/afs/cern.ch/work/y/yucekmec/private/data/scout_325117_000000.dat","r");
block bl;

json j;

int ibytes = 0;
uint32_t header;
uint32_t qual_cut = 12;
uint32_t bx;
uint32_t full;
uint32_t orbit;
while(ibytes=fread(&header,1,sizeof(uint32_t),f)){
	//fprintf(stderr, "0x%x read %d\n",header,ibytes);
	uint32_t mAcount = (header & header_masks::mAcount)>>header_shifts::mAcount;
	uint32_t mBcount = (header & header_masks::mBcount)>>header_shifts::mBcount;
	fread(&bl,1,8+(mAcount+mBcount)*8,f);
	bx = bl.bx;
	if(bx>0xf0000000) bx = bx - 0xf0000000;
      	orbit = bl.orbit;
      	if(mAcount+mBcount==16) full+=1;
	long arr[3]= {bx, orbit, mAcount+mBcount};
	j["BX, Orbit, nMuons"].push_back(arr); 
	//j["BX"]=bx;
	//j["nMuons"]=mAcount+mBcount;
	//std::cout << "Header: orbit: " << orbit << " bx: " << bx << " nMuons: " << mAcount+mBcount << std::endl;
     	for(unsigned int i=0;i<mAcount+mBcount; i++){
        	uint32_t ipt = (bl.mu[i].f >> shifts::pt) & masks::pt;
        	uint32_t qual = (bl.mu[i].f >> shifts::qual) & masks::qual;
      
        	float pt = (ipt-1)*gmt_scales::pt_scale;
      
        	float phiext = ((bl.mu[i].f >> shifts::phiext) & masks::phiext)*gmt_scales::phi_scale;
        	int32_t ietaext = ((bl.mu[i].f >> shifts::etaext) & masks::etaextv);
        	if(((bl.mu[i].f >> shifts::etaext) & masks::etaexts)!=0) ietaext -= 256;
        	float etaext = ietaext*gmt_scales::eta_scale;
     
        	uint32_t iso = (bl.mu[i].s >> shifts::iso) & masks::iso;
        	int32_t chrg = 0;
        	if((bl.mu[i].s >> shifts::chrgv) & masks::chrgv==1)
        	chrg=(bl.mu[i].s >> shifts::chrg) & masks::chrg==1 ? 1 : -1 ;
    
        	uint32_t index = (bl.mu[i].s >> shifts::index) & masks::index;
        	float phi = ((bl.mu[i].s >> shifts::phi) & masks::phi)*gmt_scales::phi_scale;
        	int32_t ieta = (bl.mu[i].s >> shifts::eta) & masks::etav;
        	if(((bl.mu[i].s >> shifts::eta) & masks::etas)!=0) ieta -= 256;
        	float eta = ieta*gmt_scales::eta_scale;
        	}
	}
std::ofstream o("output.json");
o << std::setw(4) << j << std::endl;
}
