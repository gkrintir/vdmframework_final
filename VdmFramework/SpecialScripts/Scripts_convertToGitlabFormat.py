import json, pickle, sys

#"beamcurrent"#"lumirate"#"beamposition"
fileType="beamcurrent"

GHFormatPklFileName=sys.argv[1]
GHFormatPklFile=open(GHFormatPklFileName)
GHFormatDict=pickle.load(GHFormatPklFile)
GHFormatPklFile.close()

GLFormatDict={}



if fileType is "lumirate":
    for scanName in GHFormatDict.keys():
        GLFormatDict[scanName]=[]
        for scanPoint in GHFormatDict[scanName]:
            thisScanPoint={}
            thisScanPoint["Rates"]      = scanPoint[3][0]
            thisScanPoint["RateErrs"]   = scanPoint[3][1]
            thisScanPoint["ScanName"]   = str(scanPoint[1])
            thisScanPoint["ScanNumber"] = scanPoint[0]
            thisScanPoint["ScanPoint"]  = int(scanPoint[2])
            GLFormatDict[scanName].append(thisScanPoint)
elif fileType is "beamcurrent":
    for scanName in GHFormatDict.keys():
        GLFormatDict[scanName]=[]
        for scanPoint in GHFormatDict[scanName]:
            thisScanPoint={}
            thisScanPoint['ScanNumber']   = scanPoint[0]
            thisScanPoint['ScanName']     = scanPoint[1]
            thisScanPoint['ScanPoint']    = scanPoint[2]
            thisScanPoint['dcctB1']       = scanPoint[3]
            thisScanPoint['dcctB2']       = scanPoint[4]
            thisScanPoint['sumavrgfbct1'] = scanPoint[5]
            thisScanPoint['sumavrgfbct2'] = scanPoint[6]
            thisScanPoint['fbctB1']       = scanPoint[7]
            thisScanPoint['fbctB2']       = scanPoint[8]
            GLFormatDict[scanName].append(thisScanPoint)
elif fileType is "beamposition":
    GLFormatDict=GHFormatDict
else:
    print "fileType is",fileType,".  exiting"
    sys.exit()

GLFormatPklFile=open(GHFormatPklFileName.replace(".pkl",".json"),'w')

json.dump(GLFormatDict,GLFormatPklFile)
GLFormatPklFile.close()


