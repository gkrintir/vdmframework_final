# PATH=/nfshome0/lumipro/brilconda/bin/:$PATH 


import tables, pandas as pd, pylab as py, sys, numpy
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.dates as dates
from math import sqrt
from datetime import datetime

filen = ['6854_1806272115_1806272202.hd5','6854_1806272216_1806272231.hd5','6854_1806272231_1806272311.hd5','6854_1806272312_1806272327.hd5']
folder= '/brildata/vdmdata18/'
#file1 = '' 
#filen.append(file1)
#file2 = '6847_1530010600_1530012420.hd5' 
#filen.append(file2)
#file3 = '6847_1530012420_1806261151.hd5' 
#filen.append(file3)
#file4 = '/brildata/vdmdata18/7358_1810260724_1810260821.hd5' 
#file4 = '/brildata/vdmdata18/6847_1806261047_1806261133.hd5' 
#filen.append(file4)

bcid=1451

#bxraw_list = [[] for a in range(bcid+1)] # this range should include colliding bx for which you will lot bxraw
#timestamp_list =  []

for f in filen:
    bxraw_list = [[] for a in range(bcid+1)] # this range should include colliding bx for which you will lot bxraw
    timestamp_list =  []

    h5file = tables.open_file(folder+f)

    hist = h5file.root.pltlumizero
    #hist = h5file.root.hfetlumi


    for row in hist.iterrows():
        dt = datetime.fromtimestamp(row['timestampsec'])
        timestamp_list.append(dt)
        for bx in range(bcid+1):
            bxraw_list[bx].append(row['bxraw'][bx])
    #bxraw_list[bx].append(0.0)
    #timestamp_list.append(datetime(2017, 9, 25, 16, 43, 15))

    #print timestamp_list

    # Plot for the whole fill rates
    #plt.figure(facecolor="white")
    plt.plot_date(timestamp_list,bxraw_list[bcid],'.-') # here change bx number for colliding bx in the fill you are looking at!
    #plt.errorbar(timestamp_list,bxraw_list[bcid], yerr=0, xerr=0)
    plt.grid()
    ##plt.xaxis.set_minor_formatter(dates.DateFormatter('%H:%M:%S')) 
    plt.xlabel('Time', fontsize=16)
    plt.ylabel('mu values, HFOC', fontsize=16)
    #plt.title('Fill  6241, $\sqrt{s}$=13 TeV')
    plt.gcf().autofmt_xdate()

    plt.savefig( './'+f.split('.')[0]+'.pdf')
    plt.clf()


