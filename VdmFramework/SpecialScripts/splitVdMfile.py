import tables as t, sys,pandas as pd
from datetime import datetime

folder = '/brildata/vdmdata18/'
# fin = '7443_1811141822_1811141858.hd5'   
#split = '1811141837'

# fin = '7443_1811141837_1811141858.hd5'
# split = '1811141844'


fin = '6847_1806261047_1806261133.hd5'
splitBegin = '1806261055'
splitEnd =   '1806261130'

compr_filter = t.Filters(complevel=9, complib='blosc')
chunkshape=(100,)


chunks = fin.split('.')[0].split('_')
fout1 = '_'.join(chunks[0:-1]+[splitBegin])+'.hd5'
fout2 = '_'.join(chunks[0:1]+[splitBegin]+[splitEnd])+'.hd5'
fout3 = '_'.join(chunks[0:1]+[splitEnd]+chunks[2:])+'.hd5'

#print fout1, fout2, fout3

timestampBegin = str(int(pd.to_datetime(splitBegin,format='%y%m%d%H%M').value/1e9))
timestampEnd = str(int(pd.to_datetime(splitEnd,format='%y%m%d%H%M').value/1e9))

print timestampBegin,timestampEnd

h5in = t.open_file(folder+fin)

h5out1 = t.open_file(folder+fout1,mode='w')
h5out2 = t.open_file(folder+fout2,mode='w')
h5out3 = t.open_file(folder+fout3,mode='w')

hist= h5in.root.hfoclumi
for row in hist.iterrows():
	print row['timestampsec']
#'''
for table in h5in.iter_nodes('/'):
	descr = table.description._v_colobjects.copy()
	outtable1 = h5out1.create_table('/',table.name,descr,filters=compr_filter,chunkshape=chunkshape)
	outtable2 = h5out2.create_table('/',table.name,descr,filters=compr_filter,chunkshape=chunkshape)
	outtable3 = h5out3.create_table('/',table.name,descr,filters=compr_filter,chunkshape=chunkshape)
	table.append_where(outtable1,condition ='timestampsec < '+timestampBegin)
	table.append_where(outtable2,condition ='(timestampsec > '+timestampBegin+') & (timestampsec < '+timestampEnd+')')
	table.append_where(outtable3,condition ='timestampsec > '+timestampEnd)


h5in.close()
h5out1.close()
h5out2.close()
h5out3.close()
#'''
